import configparser
import genanki
import logging
import os
import requests
import json

from collections import namedtuple
from typing import List

URL_FORMAT = 'http://words.marugotoweb.jp/SearchCategoryAPI?&lv={level}&tp={topics}&ls={lessons}&tx=act,comp&ut=en'
CONFIG_PATH = os.path.join('.', 'application.cfg')
CACHE_DIRECTORY = os.path.join('.', 'cache')
OUTPUT_DIRECTORY = os.path.join('.', 'output')

Card = namedtuple('Card', ['id', 'kanji', 'kana', 'romaji', 'meaning'])
Config = namedtuple(
    'Config', ['deck_name', 'level', 'topics', 'lessons', 'deck_id', 'model_id'])


def run():
    ensure_directory(CACHE_DIRECTORY)
    ensure_directory(OUTPUT_DIRECTORY)

    config = readconfig()

    url = URL_FORMAT.format(
        level=config.level,
        topics=config.topics,
        lessons=config.lessons)
    raw_data = get_data(url, config.deck_id)
    cards = parse_data(raw_data)
    create_anki_deck(cards, config)
    logging.info('All done')


def ensure_directory(directory: str):
    logging.info(f'Ensuring directory {directory} is created')
    if not os.path.exists(directory):
        os.makedirs(directory)

def get_data(url: str, deck_id: str) -> object:
    logging.info(f'Getting deck data')
    cache_filename = f'cache_{deck_id}'
    cache_path = os.path.join(CACHE_DIRECTORY, cache_filename)
    cached_data = read_from_cache(cache_path)
    if cached_data:
        logging.info('Returning cached data')
        return cached_data

    logging.info(f'Getting data from {url}')
    with requests.get(url) as response:
        response.raise_for_status()
        write_to_cache(cache_path, response.text)
        data = response.json()
        return data


def read_from_cache(path: str) -> object:
    logging.info(f'Looking for cached data in {path}')
    try:
        with open(path, 'r', encoding='utf-8') as f:
            data = f.read()
            if not data:
                return None
            return json.loads(data)
    except FileNotFoundError:
        return None


def write_to_cache(path: str, data: str):
    logging.info(f'Writing cached data to {path}')
    with open(path, 'w', encoding='utf-8') as f:
        f.write(data)


def parse_data(raw_data) -> List[Card]:
    if not raw_data:
        return []

    cards = []
    for data in raw_data['DATA']:
        card = Card(data['ID'],
                    data['KANJI'],
                    data['KANA'],
                    data['ROMAJI'],
                    data['UWRD'])
        cards.append(card)
    return cards


def create_anki_deck(cards: List[Card], config: Config):
    logging.info('Creating deck')
    card_model = genanki.Model(
        config.model_id,
        'Marugoto Model',
        fields=[
            {'name': 'Kanji'},
            {'name': 'Kana'},
            {'name': 'Romaji'},
            {'name': 'Definition'}
        ],
        templates=[
            {
                'name': 'Default',
                'qfmt': (
                    '<div style="text-align: center; font-size: 36px; font-family: Prettier Kanji Font;">'
                        '<span>{{Kanji}}</h2>'
                    '</div>'
                ),
                'afmt': (
                    '<div style="text-align: center; font-size: 36px; font-family: Prettier Kanji Font;">'
                        '<span>{{FrontSide}}</h2>'
                    '</div>'
                    '<hr>'
                    '<b style="font-size: 12px;">Kana</b>'
                    '<br>'
                    '<div style="font-size: 18px; font-family: Prettier Kanji Font;">'
                        '<span>{{Kana}}</span>'
                    '</div>'
                    '<hr>'
                    '<b style="font-size: 12px;">Definition</b>'
                    '<br>'
                    '<div style="font-size: 18px; font-family: Prettier Kanji Font;">'
                        '<span>{{Definition}}</span>'
                    '</div>'
                    '<hr>'
                    '<b style="font-size: 12px;">Romaji</b>'
                    '<br>'
                    '<div style="font-size: 18px; font-family: Prettier Kanji Font;">'
                        '<span>{{Romaji}}</span>'
                    '</div>'
                )
            }
        ]
    )
    deck = genanki.Deck(
        config.deck_id,
        config.deck_name
    )
    for card in cards:
        note = genanki.Note(model=card_model, fields=[
                            card.kanji, card.kana, card.romaji, card.meaning])
        deck.add_note(note)
    deck_path = os.path.join(OUTPUT_DIRECTORY, f'{config.deck_id}.apkg')
    genanki.Package(deck).write_to_file(deck_path)
    logging.info(f'Saved deck to {deck_path}')


def readconfig():
    parser = configparser.RawConfigParser()
    parser.read(CONFIG_PATH, encoding='utf-8')
    return Config(
        parser.get('DEFAULT', 'deck_name'),
        parser.get('DEFAULT', 'level'),
        parser.get('DEFAULT', 'topics').replace(' ', ''),
        parser.get('DEFAULT', 'lessons').replace(' ', ''),
        int(parser.get('DEFAULT', 'deck_id')),
        int(parser.get('DEFAULT', 'model_id'))
    )


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    run()
